﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoverFuel : MonoBehaviour
{
    public float fuelLifeTime = 5f;

    private void Start()
    {
        Destroy(gameObject, fuelLifeTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Fuel.instance.fuelAmount = 100f;
            Destroy(gameObject);
        }
    }
}
