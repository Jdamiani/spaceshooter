﻿using System;
using UnityEngine;

using UnityEngine.UI;

public class PlayerControls : MonoBehaviour
{

    [SerializeField]
    private float shipSpeed = 10f;

    [SerializeField]
    private float fireRate = 0.5f;

    [SerializeField]
    private Text score;
    
    [SerializeField]
    private Text lives;

    [SerializeField]
    private Transform initialPosition;

    public int numberOfLifes = 3;

    public static PlayerControls instance = null;

    public int playerScore = 0;

    public GameObject projectile;

    public Transform bulletStartPosition;

    private Rigidbody2D rb;

    public GameObject deathMenu = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        rb = GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        handleShipMovement();
        handleShipRotation();
        if (Input.GetMouseButtonDown(0))
        {   
            if (Time.timeScale != 0 )
            {
                InvokeRepeating("ShootBullet",0, fireRate);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            CancelInvoke("ShootBullet");
        }

        score.text = "Score: " + playerScore.ToString();
        lives.text = "Lives: " + numberOfLifes.ToString();
    }

    private void ShootBullet()
    {
        Instantiate(projectile, bulletStartPosition.position, transform.rotation);
    }

    public void Death()
    {
        numberOfLifes --;
        gameObject.transform.position = initialPosition.position;
        
        Fuel.instance.fuelAmount = 100f;
        foreach (var enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(enemy);
        }

        foreach (var bullet in GameObject.FindGameObjectsWithTag("Bullet"))
        {
            Destroy(bullet);
        }

        if (numberOfLifes <= 0)
        {
            deathMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void handleShipRotation()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(transform.localPosition);

        float angle = -Mathf.Atan2(mousePosition.x - screenPoint.x, mousePosition.y - screenPoint.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3 (0, 0, angle));  
    }

    void handleShipMovement()
    {
        float HorizontalMovement = Input.GetAxis("Horizontal");
        float VerticalMovement = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(HorizontalMovement, VerticalMovement);
        movement *= shipSpeed * Time.deltaTime;

        transform.Translate(movement);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Enemy"))
        {
            Death();
        }
    }
}
