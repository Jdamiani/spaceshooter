﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float bulletSpeed = 2;
    
    public float bulletLifeTime = 4;
    Vector3 shootDirection;

    Rigidbody2D rigidbody2;

    private void Start()
    {
        rigidbody2 = GetComponent<Rigidbody2D>();

        shootDirection = Input.mousePosition;
        shootDirection.z = 0.0f;
        shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
        shootDirection = shootDirection-transform.position;
    }

    private void FixedUpdate()
    {
        Vector2 vel = new Vector2(shootDirection.x, shootDirection.y);
        vel = vel.normalized;
        vel *= bulletSpeed;
        rigidbody2.velocity = vel;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            PlayerControls.instance.playerScore++;
            Destroy(gameObject);
        }
        if (other.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }
}
