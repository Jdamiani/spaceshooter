﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    public float spawnPickupRatio = 5f;
    public float startSpawn = 2;

    public float maxX = 0;
    public float maxY = 0;
    public float minX = 0;
    public float minY = 0;

    public GameObject fuelObj = null;

    Vector3 position;

    void Start()
    {
        InvokeRepeating("SpawnFuel", startSpawn, spawnPickupRatio);
    }
    
    public void SpawnFuel()
    {
        float posX = Random.Range(minX, maxX);
        float posY = Random.Range(minY, maxY);
        
        position = new Vector3 (posX, posY, transform.position.z);
        Instantiate (fuelObj, position, Quaternion.identity);
    }
}
