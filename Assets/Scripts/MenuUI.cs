﻿using UnityEngine;

public class MenuUI : MonoBehaviour
{
    private bool isPaused = false;
    public GameObject pausedMenuUI;
    public GameObject startGame;
    public GameObject scoreText;
    public GameObject lifeText;
    public GameObject Slider;

    public GameObject deathMenu;

    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    private void Start()
    {   
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (startGame.activeSelf == false)

            {
                if (isPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
    }

    public void Resume()
    {
        pausedMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    void Pause()
    {
        pausedMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void GameStart()
    {
        scoreText.SetActive(true);
        startGame.SetActive(false);
        Slider.SetActive(true);
        lifeText.SetActive(true);
        Time.timeScale = 1;
    }

    public void RestartGame()
    {
        foreach (var enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(enemy);
        }

        foreach (var bullet in GameObject.FindGameObjectsWithTag("Bullet"))
        {
            Destroy(bullet);
        }

        PlayerControls.instance.playerScore = 0;
        PlayerControls.instance.numberOfLifes = 3;
        Fuel.instance.fuelAmount = 100f;
        ObjectSpawner.instance.SpawnRatio = 4;
        deathMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
