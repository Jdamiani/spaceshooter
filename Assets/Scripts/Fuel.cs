﻿using UnityEngine;
using UnityEngine.UI;

public class Fuel : MonoBehaviour
{
    public static Fuel instance;
    public float fuelAmount = 100f;
    public float burntFuel = 1f;
    public Slider slider;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = fuelAmount / 100;
        if(fuelAmount <= 0)
        {
            PlayerControls.instance.Death();
        }
    }

    void FuelBurn()
    {
        fuelAmount -= burntFuel;
    }

    private void Start()
    {
        InvokeRepeating("FuelBurn", 0, 1f);
    }
}
