﻿using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject enemyPrefab = null;

    public static ObjectSpawner instance = null;

    public float startSpawning = 0;
    public float SpawnRatio = 2;

    public float timeToIncreaseDifficult = 10f;

    private float timeSpan;
    public Transform spawnerMinPosition;
    public Transform spawnerMaxPosition;

    public bool isVertical = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        timeSpan = timeToIncreaseDifficult;
        InvokeRepeating("SpawnEnemy",startSpawning, SpawnRatio);
    }

    private void Update()
    {
        timeToIncreaseDifficult -= Time.deltaTime;
        if (timeToIncreaseDifficult <= 0)
        {
            SpawnRatio -= 0.5f;
            if(SpawnRatio <= 0.5f)
            {
                SpawnRatio = 0.5f;
            }
            timeToIncreaseDifficult = timeSpan;
            CancelInvoke("SpawnEnemy");
            InvokeRepeating("SpawnEnemy",startSpawning, SpawnRatio);
        }
    }

    private void SpawnEnemy()
    {
        float yPos = Random.Range(spawnerMinPosition.position.x, spawnerMaxPosition.position.x);
        float xPos = Random.Range(spawnerMinPosition.position.x, spawnerMaxPosition.position.x);

        if (isVertical)
        {
            yPos = Random.Range(spawnerMinPosition.position.y, spawnerMaxPosition.position.y);
            xPos = transform.position.x;
        }
        else
        {
            xPos = Random.Range(spawnerMinPosition.position.x, spawnerMaxPosition.position.x);
            yPos = transform.position.y;
        }

        Vector3 positionToSpawn = new Vector3(xPos, yPos, transform.position.z);

        Instantiate(enemyPrefab, positionToSpawn, transform.rotation);
    }
}
