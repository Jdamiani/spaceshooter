﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class EnemyController : MonoBehaviour
{
    public float enemySpeed = 4;
    public AudioSource source;
    public SpriteRenderer SR;
    public SpriteRenderer CSR;

    public Collider2D collider;

    Vector3 enemyDirection;
    Rigidbody2D rigidbody2;

    private void Start()
    {
        rigidbody2 = GetComponent<Rigidbody2D>();
        enemyDirection = Vector2.down;
        collider = GetComponent<Collider2D>();
    }

    private void Update()
    {
        // transform.position = new Vector3(transform.position.x, transform.position.y + -enemySpeed * Time.deltaTime, transform.position.z);
        Destroy(gameObject, 10f);
    }

    private void FixedUpdate()
    {
        Vector2 vel = new Vector2(enemyDirection.x * enemySpeed, enemyDirection.y * enemySpeed);
        vel = vel.normalized;
        vel = transform.TransformVector(vel) * enemySpeed * Time.deltaTime;  

        rigidbody2.velocity = vel;

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bullet"))
        {
            source.Play();
            enemySpeed = 0;
            SR.enabled = false;
            CSR.enabled = false;
            collider.enabled = false;
            Destroy(gameObject, 1f);
        }
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}
